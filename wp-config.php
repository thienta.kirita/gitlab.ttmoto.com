<?php
/**
 * Cấu hình cơ bản cho WordPress
 *
 * Trong quá trình cài đặt, file "wp-config.php" sẽ được tạo dựa trên nội dung 
 * mẫu của file này. Bạn không bắt buộc phải sử dụng giao diện web để cài đặt, 
 * chỉ cần lưu file này lại với tên "wp-config.php" và điền các thông tin cần thiết.
 *
 * File này chứa các thiết lập sau:
 *
 * * Thiết lập MySQL
 * * Các khóa bí mật
 * * Tiền tố cho các bảng database
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Thiết lập MySQL - Bạn có thể lấy các thông tin này từ host/server ** //
/** Tên database MySQL */
define( 'DB_NAME', 'ttmoto' );

/** Username của database */
define( 'DB_USER', 'root' );

/** Mật khẩu của database */
define( 'DB_PASSWORD', 'ttmoto123' );

/** Hostname của database */
define( 'DB_HOST', 'localhost' );

/** Database charset sử dụng để tạo bảng database. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Kiểu database collate. Đừng thay đổi nếu không hiểu rõ. */
define('DB_COLLATE', '');

/**#@+
 * Khóa xác thực và salt.
 *
 * Thay đổi các giá trị dưới đây thành các khóa không trùng nhau!
 * Bạn có thể tạo ra các khóa này bằng công cụ
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Bạn có thể thay đổi chúng bất cứ lúc nào để vô hiệu hóa tất cả
 * các cookie hiện có. Điều này sẽ buộc tất cả người dùng phải đăng nhập lại.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '9.XRM*jFsE[+|MX15mEpMKH/`8l5TU=aGx%>=>DB8exRRwp [m@unbgG!2^5g?1^' );
define( 'SECURE_AUTH_KEY',  '3pvaXNM86EMJ5_UJvYX}AOdH-Tl#OY|xn{t}vV}xc(q}mn<c_[[Sd)N<^1;T$fd3' );
define( 'LOGGED_IN_KEY',    'TL884VtS.45aVl]W(B(sI9xS(,rgj-tVC/wq7rfR9ByTe`:nSfQ$/K4xpu8o_sYG' );
define( 'NONCE_KEY',        '<he7g?im$-UTi!SU^iS9bFVhP#Jd.xnLD@Qiwh(ChtwcDbF8FTq@to-UHLl-pQJp' );
define( 'AUTH_SALT',        '`#b?A%6>5k!sO<2{KR4lO%Sk^HI?>cbXj{?+)h)PU_TVho3`/PA-x,#UT7;{3#Df' );
define( 'SECURE_AUTH_SALT', '_>[Tc/*}2b(;Zx/w{SgK~b!aTo?Lw,_DJDN]=)b@<A @jE1]Y<**u^2jT5oxeNPB' );
define( 'LOGGED_IN_SALT',   '] pZve>+Woj:2V )Y{ue(ENV5aIC,{=~)*xVE~@>`ubyTK{QL 4ENd,fQmdROoL7' );
define( 'NONCE_SALT',       'us9Qwf$q-{mD;a[9J1XRv5D)!T+iJ?TY/@6AuAnE%YQ0Vn/P~lOr9IelO^Py*);A' );

/**#@-*/

/**
 * Tiền tố cho bảng database.
 *
 * Đặt tiền tố cho bảng giúp bạn có thể cài nhiều site WordPress vào cùng một database.
 * Chỉ sử dụng số, ký tự và dấu gạch dưới!
 */
$table_prefix  = 'wp_';

/**
 * Dành cho developer: Chế độ debug.
 *
 * Thay đổi hằng số này thành true sẽ làm hiện lên các thông báo trong quá trình phát triển.
 * Chúng tôi khuyến cáo các developer sử dụng WP_DEBUG trong quá trình phát triển plugin và theme.
 *
 * Để có thông tin về các hằng số khác có thể sử dụng khi debug, hãy xem tại Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Đó là tất cả thiết lập, ngưng sửa từ phần này trở xuống. Chúc bạn viết blog vui vẻ. */

/** Đường dẫn tuyệt đối đến thư mục cài đặt WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Thiết lập biến và include file. */
require_once(ABSPATH . 'wp-settings.php');
